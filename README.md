# yolov2-pytorch

Yolov2 implementation in Pytorch. The most code is modified from [yolov2.pytorch](https://gitee.com/zhouguanghui001/yolov2.pytorch.git).

## Prepare data
In the `data` directory
```
bash get_data.sh
bash make_text.sh
```

## Train a model
Train on [autodl.com](https://www.autodl.com/home), about 9 hours
- PyTorch 2.0.0, Python 3.8(ubuntu20.04), Cuda 11.8
- GPU: RTX 2080 Ti(11GB) * 1
- CPU: 12 vCPU Intel(R) Xeon(R) Platinum 8255C CPU @ 2.50GHz
- MEM: 40GB

```
python train.py --dataset data/train.txt
```

## Inference
```
python inference.py data/VOCdevkit/VOC2007/JPEGImages/000002.jpg  # use `output/yolov2_epoch_160.pth`
python inference.py data/VOCdevkit/VOC2007/JPEGImages/000002.jpg --darknet output/yolo-voc.weights
```

## Resources
  - [YOLO9000: Better, Faster, Stronger](https://arxiv.org/abs/1612.08242)
  - https://www.bilibili.com/video/BV1NK41127TH/?vd_source=7d2f3e623fe725951776a6167dc1ece9
  - https://gitee.com/zhouguanghui001/yolov2.pytorch.git
  - https://github.com/uvipen/Yolo-v2-pytorch
  