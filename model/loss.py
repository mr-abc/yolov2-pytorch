# --------------------------------------------------------
# Pytorch Yolov2
# Licensed under The MIT License [see LICENSE for details]
# Written by Jingru Tan
# --------------------------------------------------------
import torch
import torch.nn as nn
import torch.nn.functional as F
import model.config as cfg

from model.utils.bbox import (
    generate_all_anchors,
    xywh2xxyy,
    box_transform_inv,
    box_ious,
    xxyy2xywh,
    box_transform,
)


class YoloLoss(nn.Module):
    """
    Calculate the loss for yolo (v2) model
    """

    def __init__(self, num_anchors=5):
        super(YoloLoss, self).__init__()
        self.num_anchors = num_anchors

    def forward(self, x, gt_boxes=None, gt_classes=None, num_boxes=None):
        gt_data = (gt_boxes, gt_classes, num_boxes)
        target_data = build_target(x, gt_data, self.num_anchors)
        box_loss, iou_loss, class_loss = yolo_loss(x, target_data)

        return box_loss, iou_loss, class_loss


def build_target(output_data, gt_data, num_anchors):
    """
    Build the training target for output tensor

    Arguments:
      output_data
        - output data of the yolo network
        - map of delta_pred, conf_pred, class_pred, height and width
          - delta_pred
            - predictions of σ(t_x), σ(t_y), σ(t_w), σ(t_h)
            - (B, H * W * num_anchors, 4)
          - conf_pred
            - prediction of IoU score σ(t_c)
            - (B, H * W * num_anchors, 1)
          - class_score
            - prediction of class scores (cls1, cls2, ..)
            - (B, H * W * num_anchors, num_classes)
      gt_data
        - ground truth data
        - tuple of gt_boxes_batch, gt_classes_batch, num_boxes_batch
          - gt_boxes_batch
            - # of ground truth boxes
            - normalized values (x1, y1, x2, y2) range 0~1
            - (B, N, 4)
          - gt_classes_batch
            - # of ground truth classes
            - (B, N)
          - num_obj_batch
            - # of objects
            - (B, 1)

    Returns:
        iou_target   : (B, H * W * num_anchors, 1)
        iou_mask     : (B, H * W * num_anchors, 1)
        box_target   : (B, H * W * num_anchors, 4)
        box_mask     : (B, H * W * num_anchors, 1)
        class_target : (B, H * W * num_anchors, 1)
        class_mask   : (B, H * W * num_anchors, 1)
    """
    delta_pred_batch = output_data["delta_pred"].data
    conf_pred_batch = output_data["conf_pred"].data
    H = output_data["height"]
    W = output_data["width"]

    gt_boxes_batch = gt_data[0]
    gt_classes_batch = gt_data[1]
    num_boxes_batch = gt_data[2]

    bsize = delta_pred_batch.size(0)

    # initial the output tensor
    # we use `tensor.new()` to make the created tensor has the same devices and
    # data type as input tensor, what tensor is used doesn't matter
    # fmt: off
    iou_target = delta_pred_batch.new_zeros((bsize, H * W, num_anchors, 1))
    iou_mask = delta_pred_batch.new_ones((bsize, H * W, num_anchors, 1)) * cfg.noobject_scale
    box_target = delta_pred_batch.new_zeros((bsize, H * W, num_anchors, 4))
    box_mask = delta_pred_batch.new_zeros((bsize, H * W, num_anchors, 1))
    class_target = conf_pred_batch.new_zeros((bsize, H * W, num_anchors, 1))
    class_mask = conf_pred_batch.new_zeros((bsize, H * W, num_anchors, 1))
    # fmt: on

    # get all the anchors

    anchors = torch.FloatTensor(cfg.anchors)

    # note that the all anchors' xywh scale is normalized by the grid width and
    # height, i.e. 13 x 13. this is very crucial because the predict output is
    # normalized to 0~1, which is also normalized by the grid width and height

    # shape: (H * W * num_anchors, 4), format: (x, y, w, h)
    all_grid_xywh = generate_all_anchors(anchors, H, W)
    all_grid_xywh = delta_pred_batch.new(*all_grid_xywh.size()).copy_(all_grid_xywh)
    all_anchors_xywh = all_grid_xywh.clone()
    all_anchors_xywh[:, 0:2] += 0.5
    if cfg.debug:
        print("all grid: ", all_grid_xywh[:12, :])
        print("all anchor: ", all_anchors_xywh[:12, :])
    all_anchors_xxyy = xywh2xxyy(all_anchors_xywh)

    # process over batches
    for b in range(bsize):
        num_obj = num_boxes_batch[b].item()
        delta_pred = delta_pred_batch[b]
        gt_boxes = gt_boxes_batch[b][:num_obj, :]
        gt_classes = gt_classes_batch[b][:num_obj]

        # rescale ground truth boxes
        gt_boxes[:, 0::2] *= W
        gt_boxes[:, 1::2] *= H

        # step 1: process IoU target

        # apply delta_pred to pre-defined anchors
        all_anchors_xywh = all_anchors_xywh.view(-1, 4)
        box_pred = box_transform_inv(all_grid_xywh, delta_pred)
        box_pred = xywh2xxyy(box_pred)

        # for each anchor, its iou target is corresponded to the max iou with
        # any gt boxes
        ious = box_ious(box_pred, gt_boxes)  # shape: (H * W * num_anchors, num_obj)
        ious = ious.view(-1, num_anchors, num_obj)
        # shape: (H * W, num_anchors, 1)
        max_iou, _ = torch.max(ious, dim=-1, keepdim=True)
        if cfg.debug:
            print("ious", ious)

        # iou_target[b] = max_iou

        # we ignore the gradient of predicted boxes whose IoU with any gt box
        # is greater than cfg.threshold
        iou_thresh_filter = max_iou.view(-1) > cfg.thresh
        n_pos = torch.nonzero(iou_thresh_filter).numel()

        if n_pos > 0:
            iou_mask[b][max_iou >= cfg.thresh] = 0

        # step 2: process box target and class target

        # calculate overlaps between anchors and gt boxes
        overlaps = box_ious(all_anchors_xxyy, gt_boxes).view(-1, num_anchors, num_obj)
        gt_boxes_xywh = xxyy2xywh(gt_boxes)

        # iterate over all objects
        for t in range(gt_boxes.size(0)):
            # compute the center of each gt box to determine which cell it falls
            # on assign it to a specific anchor by choosing max IoU

            gt_box_xywh = gt_boxes_xywh[t]
            gt_class = gt_classes[t]
            cell_idx_x, cell_idx_y = torch.floor(gt_box_xywh[:2])
            cell_idx = cell_idx_y * W + cell_idx_x
            cell_idx = cell_idx.long()

            # update box_target, box_mask
            overlaps_in_cell = overlaps[cell_idx, :, t]
            argmax_anchor_idx = torch.argmax(overlaps_in_cell)

            assigned_grid = all_grid_xywh.view(-1, num_anchors, 4)[
                cell_idx, argmax_anchor_idx, :
            ].unsqueeze(0)
            gt_box = gt_box_xywh.unsqueeze(0)
            target_t = box_transform(assigned_grid, gt_box)
            if cfg.debug:
                print("assigned_grid, ", assigned_grid)
                print("gt: ", gt_box)
                print("target_t, ", target_t)
            box_target[b, cell_idx, argmax_anchor_idx, :] = target_t.unsqueeze(0)
            box_mask[b, cell_idx, argmax_anchor_idx, :] = 1

            # update cls_target, cls_mask
            class_target[b, cell_idx, argmax_anchor_idx, :] = gt_class
            class_mask[b, cell_idx, argmax_anchor_idx, :] = 1

            # update iou target and iou mask
            iou_target[b, cell_idx, argmax_anchor_idx, :] = max_iou[
                cell_idx, argmax_anchor_idx, :
            ]
            if cfg.debug:
                print(max_iou[cell_idx, argmax_anchor_idx, :])
            iou_mask[b, cell_idx, argmax_anchor_idx, :] = cfg.object_scale

    return (
        iou_target.view(bsize, -1, 1),
        iou_mask.view(bsize, -1, 1),
        box_target.view(bsize, -1, 4),
        box_mask.view(bsize, -1, 1),
        class_target.view(bsize, -1, 1).long(),
        class_mask.view(bsize, -1, 1),
    )


def yolo_loss(output, target):
    """
    Build yolo loss

    Arguments:
        output
          - output data of the yolo network
          - map of delta_pred, conf_pred, class_score
            - delta_pred
            - predictions of σ(t_x), σ(t_y), σ(t_w), σ(t_h)
            - (B, H * W * num_anchors, 4)
          - conf_pred
            - prediction of IoU score σ(t_c)
            - (B, H * W * num_anchors, 1)
          - class_score
            - prediction of class scores (cls1, cls2, ..)
            - (B, H * W * num_anchors, num_classes)
        target
          - target label data
          - tuple of iou_target, iou_mask, box_target, box_mask, class_target, class_mask
            - iou_target   : (B, H * W * num_anchors, 1)
            - iou_mask     : (B, H * W * num_anchors, 1)
            - box_target   : (B, H * W * num_anchors, 4)
            - box_mask     : (B, H * W * num_anchors, 1)
            - class_target : (B, H * W * num_anchors, 1)
            - class_mask   : (B, H * W * num_anchors, 1)

    Return:
        loss -- yolo overall multi-task loss
    """

    delta_pred_batch = output["delta_pred"]
    conf_pred_batch = output["conf_pred"]
    class_score_batch = output["class_score"]
    iou_target, iou_mask, box_target, box_mask, class_target, class_mask = target

    b, _, num_classes = class_score_batch.size()
    class_score_batch = class_score_batch.view(-1, num_classes)
    class_target = class_target.view(-1)
    class_mask = class_mask.view(-1)

    # ignore the gradient of noobject's target
    class_keep = class_mask.nonzero().squeeze(1)
    class_score_batch_keep = class_score_batch[class_keep, :]
    class_target_keep = class_target[class_keep]

    # if cfg.debug:
    #     print(class_score_batch_keep)
    #     print(class_target_keep)

    # calculate the loss, normalized by batch size.
    # fmt: off
    box_loss = 1 / b * cfg.coord_scale * F.mse_loss(delta_pred_batch * box_mask, box_target * box_mask, reduction='sum') / 2.0
    iou_loss = 1 / b * F.mse_loss(conf_pred_batch * iou_mask, iou_target * iou_mask, reduction='sum') / 2.0
    class_loss = 1 / b * cfg.class_scale * F.cross_entropy(class_score_batch_keep, class_target_keep, reduction='sum')
    # fmt:on

    return box_loss, iou_loss, class_loss
