import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import torch
from model.darknet import Darknet19

model = Darknet19()
input = torch.rand((1, 3, 224, 224))
output = model(input)
print(output.shape)
print(model)
