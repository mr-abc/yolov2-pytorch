import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import torch
from model.model import Yolov2

model = Yolov2()
input = torch.rand((1, 3, 416, 416))
output = model(input)
print(output.shape)
print(model)
