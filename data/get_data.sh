#!/usr/bin/env bash

## DOWNLOAD from JOSEPHS WEBSITE (SLOWER DOWNLOAD)                                 
#wget https://pjreddie.com/media/files/VOCtrainval_11-May-2012.tar
#wget https://pjreddie.com/media/files/VOCtrainval_06-Nov-2007.tar
#wget https://pjreddie.com/media/files/VOCtest_06-Nov-2007.tar    
                                                              
## OR DOWNLOAD FROM HERE (FASTER DOWNLOAD)                                          
# VOC2007 DATASET                                                              
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtrainval_06-Nov-2007.tar
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCtest_06-Nov-2007.tar

# VOC2012 DATASET                                                              
wget http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar

# Extract tar files
tar xf VOCtrainval_11-May-2012.tar
tar xf VOCtrainval_06-Nov-2007.tar
tar xf VOCtest_06-Nov-2007.tar
