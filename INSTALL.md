## Requirements

```
conda create -n yolov2 python=3.10 -y
conda activate yolov2

pip install numpy
pip install matplotlib
pip install Pillow
pip install tensorboardx
pip install opencv-python
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
```